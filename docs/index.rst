.. hdlmake documentation master file, created by
   sphinx-quickstart on Thu Oct  2 12:02:32 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to hdlmake's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Features
--------

- Synthesis
- Simulation
- GIT/SVN Support
- Multi Language
- Multi Tools
- Multiple Operating System Support


Installation
------------

hdlmake is a Python application.


Contribute
----------

- Issue Tracker: http://www.ohwr.org/projects/hdl-make/issues
- Source Code: http://www.ohwr.org/projects/hdl-make/repository


Support
-------

If you are having issues, please let us know.
We have a mailing list located at: 
http://www.ohwr.org/mailing_list/show?project_id=hdl-make

License
-------

The project is licensed under the GPL v3 license.
